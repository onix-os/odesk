package core

import (
	"encoding/json"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/onix-os/odesk/configdir"
	"gitlab.com/onix-os/odesk/xgbutil"
)

// Core package: Contains core libraries and utilities like bootup scripts.

var (
	// Logger for error and debug messages.
	Logger = log.New(os.Stderr, "ODESK: ", log.Lshortfile)
)

var defaults = map[string]string{
	"theme": "default",
	"autostart": "autostart.json",
}

type Config struct {
	Theme     string `json:"theme"`
	AutoStart string `json:"autostart"`
}

type AutoStart struct {
	Name    string `json:"name"`
	Command string `json:"command"`
}

var Defaults = Config{
	Theme:     "default",
	AutoStart: "autostart.json",
}

type Core struct {
	Connection *xgbutil.XUtil
	Error      error
	Config     Config
	File       string
}

// Init initializes the X11 connection.
func (c *Core) Init() {
	Logger.Printf("Display: %v", os.Getenv("DISPLAY"))
	var err error
	c.Connection, err = xgbutil.NewConn()
	if err != nil {
		c.Error = err
		Logger.Printf("Failed to connect to X11: %v", err)
		return
	}

}

// MakeInitialConfig reads or initializes the system configuration file.
func (c *Core) MakeInitialConfig() {
	confdir := configdir.New("onixos", "odesk")
	confdir.LocalPath, _ = filepath.Abs(".")
	folder := confdir.QueryFolderContainsFile(c.File)

	if folder != nil {
		data, err := folder.ReadFile("system.json")
		if err != nil || data == nil {
			Logger.Printf("Failed to read system.json: %v", err)
			return // Eğer dosya yoksa ya da hata varsa işlemi pas geç
		}

		var config Config
		if err := json.Unmarshal(data, &config); err != nil {
			Logger.Printf("Failed to parse configuration: %v", err)
			return // Parsing sırasında hata oluşursa işlemi pas geç
		}

		folders := confdir.QueryFolders(configdir.Local)
		if len(folders) > 0 {
			folders[0].WriteFile("system.json", data)
		}
		c.Config = config
		Logger.Println("Configuration loaded successfully.")
	} else {
		Logger.Println("No existing configuration found.")
	}
}

// MakeAutoStartFile reads or initializes the autostart configuration.
func (c *Core) MakeAutoStartFile() {
	confdir := configdir.New("onixos", "odesk")
	confdir.LocalPath, _ = filepath.Abs(".")
	folder := confdir.QueryFolderContainsFile(c.Config.AutoStart)

	if folder != nil {
		data, err := folder.ReadFile(c.Config.AutoStart)
		if err != nil || data == nil {
			Logger.Printf("Failed to read autostart file: %v", err)
			return // Eğer dosya yoksa ya da hata varsa işlemi pas geç
		}

		var autostart []AutoStart
		if err := json.Unmarshal(data, &autostart); err != nil {
			Logger.Printf("Failed to parse autostart file: %v", err)
			return // Parsing sırasında hata oluşursa işlemi pas geç
		}

		folders := confdir.QueryFolders(configdir.Local)
		if len(folders) > 0 {
			folders[0].WriteFile(c.Config.AutoStart, data)
		}
		Logger.Println("Autostart configuration loaded successfully.")
	} else {
		Logger.Println("No existing autostart file found.")
	}
}
