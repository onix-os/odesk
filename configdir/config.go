package configdir

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

// ConfigType defines different configuration folder types.
type ConfigType int

const (
	System ConfigType = iota
	Global
	All
	Existing
	Local
	Cache
)

// Logger is used for error and debug messages.
var Logger = log.New(os.Stderr, "CONFIGDIR: ", log.Lshortfile)

// Config represents a configuration folder.
type Config struct {
	Path string
	Type ConfigType
}

// Open opens a file in the config folder.
func (c Config) Open(fileName string) (*os.File, error) {
	fullPath := filepath.Join(c.Path, fileName)
	Logger.Printf("Opening file: %v", fullPath)
	return os.Open(fullPath)
}

// Create creates a file in the config folder.
func (c Config) Create(fileName string) (*os.File, error) {
	fullPath := filepath.Join(c.Path, fileName)
	Logger.Printf("Creating file: %v", fullPath)

	if err := c.CreateParentDir(fileName); err != nil {
		return nil, err
	}
	return os.Create(fullPath)
}

// ReadFile reads the contents of a file in the config folder.
func (c Config) ReadFile(fileName string) ([]byte, error) {
	fullPath := filepath.Join(c.Path, fileName)
	Logger.Printf("Reading file: %v", fullPath)
	return os.ReadFile(fullPath)
}

// CreateParentDir ensures the parent directory for the given file exists.
func (c Config) CreateParentDir(fileName string) error {
	fullPath := filepath.Join(c.Path, fileName)
	dir := filepath.Dir(fullPath)
	Logger.Printf("Creating parent directory: %v", dir)
	return os.MkdirAll(dir, 0755)
}

// WriteFile writes data to a file in the config folder.
func (c Config) WriteFile(fileName string, data []byte) error {
	fullPath := filepath.Join(c.Path, fileName)
	Logger.Printf("Writing to file: %v", fullPath)

	if err := c.CreateParentDir(fileName); err != nil {
		return err
	}
	return os.WriteFile(fullPath, data, 0644)
}

// MkdirAll ensures the config folder exists.
func (c Config) MkdirAll() error {
	Logger.Printf("Ensuring directory exists: %v", c.Path)
	return os.MkdirAll(c.Path, 0755)
}

// Exists checks if a file exists in the config folder.
func (c Config) Exists(fileName string) bool {
	fullPath := filepath.Join(c.Path, fileName)
	Logger.Printf("Checking if file exists: %v", fullPath)
	_, err := os.Stat(fullPath)
	return !os.IsNotExist(err)
}

// ConfigDir holds settings for querying configuration folders.
type ConfigDir struct {
	VendorName      string
	ApplicationName string
	LocalPath       string
	Path            string
}

// New initializes a new ConfigDir instance.
func New(vendorName, applicationName string) ConfigDir {
	Logger.Printf("Initializing ConfigDir: VendorName=%v, ApplicationName=%v", vendorName, applicationName)
	return ConfigDir{
		VendorName:      vendorName,
		ApplicationName: applicationName,
	}
}

func (c ConfigDir) joinPath(root string) string {
	if c.VendorName != "" {
		return filepath.Join(root, c.VendorName, c.ApplicationName)
	}
	return filepath.Join(root, c.ApplicationName)
}

// QueryFolders returns a list of configuration folders based on the type.
func (c ConfigDir) QueryFolders(configType ConfigType) []*Config {
	var result []*Config

	if configType == Cache {
		return []*Config{c.QueryCacheFolder()}
	}

	if c.LocalPath != "" && configType != System && configType != Global {
		result = append(result, &Config{
			Path: c.LocalPath,
			Type: Local,
		})
	}

	if configType != System && configType != Local {
		result = append(result, &Config{
			Path: c.joinPath(globalSettingFolder),
			Type: Global,
		})
	}

	if configType != Global && configType != Local {
		for _, root := range systemSettingFolders {
			result = append(result, &Config{
				Path: c.joinPath(root),
				Type: System,
			})
		}
	}

	if configType == Existing {
		var existing []*Config
		for _, entry := range result {
			if _, err := os.Stat(entry.Path); !os.IsNotExist(err) {
				existing = append(existing, entry)
			}
		}
		return existing
	}

	return result
}

// QueryFolderContainsFile finds a folder that contains the specified file.
func (c ConfigDir) QueryFolderContainsFile(fileName string) *Config {
	for _, config := range c.QueryFolders(Existing) {
		if _, err := os.Stat(filepath.Join(config.Path, fileName)); !os.IsNotExist(err) {
			Logger.Printf("Found file in folder: %v", config.Path)
			return config
		}
	}
	return nil
}

// QueryCacheFolder returns the cache folder.
func (c ConfigDir) QueryCacheFolder() *Config {
	return &Config{
		Path: c.joinPath(cacheFolder),
		Type: Cache,
	}
}

func (c ConfigDir) Exec(command string, prepend ...string) {
	// Kullanıcının ana dizinini al
	homeDir, err := os.UserHomeDir()
	if err != nil {
		Logger.Printf("Failed to get home directory: %v", err)
		return
	}

	// Yolu $HOME/.config/onixos/odesk/command.sh şeklinde oluştur
	execPath := filepath.Join(homeDir, ".config", c.VendorName, c.ApplicationName, "bin", command)

	// Prepend'i kontrol et
	var prefix string
	if len(prepend) > 0 && prepend[0] != "" {
		prefix = prepend[0] + " "
	}

	fullCommand := fmt.Sprintf("%s%s", prefix, execPath)
	Logger.Printf("Executing command: %v", fullCommand)

	go func() {
		cmd := exec.Command("/bin/sh", "-c", fullCommand) // Shell üzerinden çalıştır
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Start(); err != nil {
			Logger.Printf("Failed to start command %v: %v", command, err)
		}
	}()
}

// GetPath combines the base path with a given subpath.
func (c ConfigDir) GetPath(subPath string) string {

	homeDir, err := os.UserHomeDir()
	if err != nil {
		Logger.Printf("Failed to get home directory: %v", err)
		return ""
	}

	// Yolu $HOME/.config/onixos/odesk/command.sh şeklinde oluştur
	fullPath := filepath.Join(homeDir, ".config", c.VendorName, c.ApplicationName, subPath)
	Logger.Printf("Generated path: %v", fullPath)
	return fullPath
}
