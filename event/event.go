package event

/*
Simple windows events; click, move, minimize, maximize, close, double-click, open-app etc.
*/

import (
  "log"
  "gitlab.com/onix-os/odesk/xgb/xproto"
	"gitlab.com/onix-os/odesk/xgbutil"
	"gitlab.com/onix-os/odesk/xgbutil/xcursor"
)

type Event struct{
  Id interface{}
  Name string
  Class interface{}
} 

type Events []Event

type Cursors struct{
  LeftPtr           xproto.Cursor
	Fleur             xproto.Cursor
	Watch             xproto.Cursor
	TopSide           xproto.Cursor
	TopRightCorner    xproto.Cursor
	RightSide         xproto.Cursor
	BottomRightCorner xproto.Cursor
	BottomSide        xproto.Cursor
	BottomLeftCorner  xproto.Cursor
	LeftSide          xproto.Cursor
	TopLeftCorner     xproto.Cursor
}


func InitializeCursors(X *xgbutil.XUtil) *Cursors{
	cc := func(cursor uint16) xproto.Cursor {
		cid, err := xcursor.CreateCursor(X, cursor)
		if err != nil {
			log.Printf("Could not load cursor '%d'.", cursor)
			return 0
		}
		return cid
	}
  
  cursor := &Cursors{}

	cursor.LeftPtr = cc(xcursor.LeftPtr)
	cursor.Fleur = cc(xcursor.Fleur)
	cursor.Watch = cc(xcursor.Watch)
	cursor.TopSide = cc(xcursor.TopSide)
	cursor.TopRightCorner = cc(xcursor.TopRightCorner)
	cursor.RightSide = cc(xcursor.RightSide)
	cursor.BottomRightCorner = cc(xcursor.BottomRightCorner)
	cursor.BottomSide = cc(xcursor.BottomSide)
	cursor.BottomLeftCorner = cc(xcursor.BottomLeftCorner)
	cursor.LeftSide = cc(xcursor.LeftSide)
	cursor.TopLeftCorner = cc(xcursor.TopLeftCorner)
  
  return cursor
}


func RegisterEvents(x *xgbutil.XUtil) {
  /* read config file and add every line to event key press etc */
}
