# Define variables
CONFIG_DIR := $(HOME)/.config/onixos/odesk
BIN_DIR := $(CONFIG_DIR)/bin
ODESK_BIN := odesk
CURRENT_DIR := $(CURDIR)
CURRENT_DATETIME := $(shell date "+%Y%m%d%H%M%S")

# Default target
all: clean build

dev: update clean config_clean build config xinitrc

# Update repository
update:
	git pull

# Create configuration directories and copy files
config:
	mkdir -p $(CONFIG_DIR) $(BIN_DIR)
	cp -av $(CURRENT_DIR)/_samples/config/* $(CONFIG_DIR)/
	cp $(CURRENT_DIR)/$(ODESK_BIN) $(BIN_DIR)/$(ODESK_BIN)

# Append execution command to .xinitrc
xinitrc:
#	@touch $(HOME)/.xinitrc
	@cp -v $(HOME)/.xinitrc $(HOME)/.xinitrc.$(CURRENT_DATETIME).bak
	@cp -av $(CURRENT_DIR)/_samples/xinitrc.txt $(HOME)/.xinitrc
#	@grep -qxF "exec $(BIN_DIR)/$(ODESK_BIN) > $(CONFIG_DIR)/startup.log 2>&1" $(HOME)/.xinitrc || \
#	echo "exec $(BIN_DIR)/$(ODESK_BIN) > $(CONFIG_DIR)/startup.log 2>&1" > $(HOME)/.xinitrc

# Build the Go project and copy the binary
build:
	go build -v
	

# Clean up build artifacts
config_clean:
	rm -rfv $(CONFIG_DIR)

clean:
	rm -rfv odesk

dev-test:
	$(BIN_DIR)/$(ODESK_BIN) > $(CONFIG_DIR)/startup.log

# Phony targets
.PHONY: all update config xinitrc build clean
