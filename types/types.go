package types

type Icon struct {
	MimeType string
	Path     string
	Type     string
	Size     int64
	Width    int64
	Height   int64
}

type Variable struct {
	Key   interface{}
	Value interface{}
}

type Config struct {
	Path      string
	Variables []Variable
}

type App struct {
	Icon      Icon
	Name      string
	Shortname string
	Command   string
	Workdir   string
	Params    []string
}

type Image struct {
	Path    string
	Type    interface{}
	Details interface{}
}

type Window struct {
	Height     int64
	Width      int64
	Fullscreen bool
	Maximized  bool
	Minimized  bool
	X          int64
	Y          int64
}

type Clock struct {
	Unixtime interface{}
	Format   bool
	Output   string
}

type Notification struct{}

type Workspace struct {
	Id      interface{}
	Name    string
	Windows []Window
}

type MinimizedWindows struct {
	Id      interface{}
	Windows []Window
}

type Shortcuts struct {
	Id   interface{}
	Apps []App
}

type Detects struct {
	Id      interface{}
	Objects interface{}
}

type File struct {
	MimeType interface{}
	Content  interface{}
	Name     interface{}
}

type Files struct {
	Path interface{}
	List []Files
}

type Desklet struct {
	Config         interface{}
	Name           string
	PluginName     string
	CannonicalName string
	Basefile       string
}
