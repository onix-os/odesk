package main

import (
	"gitlab.com/onix-os/odesk/desktop"
)

func main() {
	desktop := desktop.Desktop{}
	desktop.Initialize()

	ipc(desktop.Core.Connection)
}
