package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"path"

	"gitlab.com/onix-os/odesk/xgbutil"
)

var (
	// Logger for error and debug messages.
	Logger = log.New(os.Stderr, "ODESK: ", log.Lshortfile)
)

func ipc(X *xgbutil.XUtil) {
	fpath := socketFilePath(X)

	os.Remove(fpath)

	listener, err := net.Listen("unix", fpath)
	if err != nil {
		Logger.Printf("Could not start IPC listener: %s", err)
	}
	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			Logger.Printf("Error accepting IPC connection: %s", err)
			continue
		}
		go handleClient(conn)
	}
}

func socketFilePath(X *xgbutil.XUtil) string {
	if X == nil {
		Logger.Printf("X connection error: %+v\n", X)
		return ""
	}
	xc := X.Conn()
	name := fmt.Sprintf(":%d.%d", xc.DisplayNumber, xc.DefaultScreen)

	var runtimeDir string
	xdgRuntime := os.Getenv("XDG_RUNTIME_DIR")
	if len(xdgRuntime) > 0 {
		runtimeDir = path.Join(xdgRuntime, "onixui")
	} else {
		runtimeDir = path.Join(os.TempDir(), "onixui")
	}

	if err := os.MkdirAll(runtimeDir, 0777); err != nil {
		Logger.Fatalf("Could not create directory '%s': %s",
			runtimeDir, err)
	}

	return path.Join(runtimeDir, name)
}

func handleClient(conn net.Conn) {
	defer conn.Close()
	for {
		reader := bufio.NewReader(conn)
		msg, err := reader.ReadString(0)
		if err == io.EOF {
			return
		}
		if err != nil {
			Logger.Printf("Error reading command '%s': %s", msg, err)
			return
		}
		msg = msg[:len(msg)-1] // get rid of null terminator

		Logger.Printf("Running command from IPC: '%s'.", msg)

		/*
			val, err := commands.Env.RunMany(msg)
			if err != nil {
				log.Printf("ERROR running command: '%s'.", err)
				fmt.Fprintf(conn, "ERROR: %s%c", err, 0)

				continue
			}

			if val != nil {
				var retVal string
				switch v := val.(type) {
				case string:
					retVal = v
				case int:
					retVal = fmt.Sprintf("%d", v)
				case float64:
					retVal = fmt.Sprintf("%f", v)
				default:
					logger.Error.Fatalf("BUG: Unknown Gribble return type: %T", v)
				}
				fmt.Fprintf(conn, "%s%c", retVal, 0)
			} else {
				fmt.Fprintf(conn, "%c", 0)
			}
		*/
	}
}
