package desktop

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"image"
	"image/png"
	"reflect"

	"gitlab.com/onix-os/odesk/configdir"
	"gitlab.com/onix-os/odesk/core"
	"gitlab.com/onix-os/odesk/types"
	"gitlab.com/onix-os/odesk/xgb/xproto"
	"gitlab.com/onix-os/odesk/xgbutil"
	"gitlab.com/onix-os/odesk/xgbutil/keybind"
	"gitlab.com/onix-os/odesk/xgbutil/mousebind"
	"gitlab.com/onix-os/odesk/xgbutil/xevent"
	"gitlab.com/onix-os/odesk/xgbutil/xwindow"
)

var (
	// Logger for error and debug messages.
	Logger = log.New(os.Stderr, "ODESK: ", log.Lshortfile)
)

type Desktop struct {
	Wallpaper    *types.Image
	Applications []*types.App
	Dock         Dock
	Config       *types.Config
	Core         *core.Core
	ConfDir		*configdir.ConfigDir
	SystemConfig *Config
	Setup		*xproto.SetupInfo
	Screen		*xproto.ScreenInfo
	Root		xproto.Window
}

type Dock struct {
	Launcher         *types.App
	WindowList       []*types.Window
	NotificationList []*types.Notification
	Shortcuts        *types.Shortcuts
	Clock            *types.Clock
}

type Config struct {
	Shortcuts []Shortcut `json:"shortcuts"`
	AutoStart []AutoStart `json:"startup"`
	Theme string `json:"theme"`
	Menu []Menu `json:"menu"`
	Bar []Bar `json:"bar"`
}

type Shortcut struct {
	Key     string `json:"key"`
	Command string `json:"command"`
	Type    string `json:"type"`
}


type AutoStart struct {
	Command string `json:"command"`
	Type    string `json:"type"`
}

type Menu struct {
	Name string `json:"name"`
	Command string `json:"command"`
	Type    string `json:"type"`
}

type Bar struct {
	Name string `json:"name"`
	Icon string `json:"icon"`
	Command string `json:"command"`
	Type    string `json:"type"`
}

// Initialize initializes the Core and sets up keybindings
func (desk *Desktop) Initialize() {
	// Initialize Core
	c := &core.Core{File: "system.json"}
	conf := configdir.New("onixos", "odesk")

	_, systemCheck := os.ReadFile(conf.GetPath("system.json"))

	if systemCheck != nil{
		c.MakeInitialConfig()
	}

	desk.Core = c
	desk.ConfDir = &conf
	
	

	readConfig, err := desk.ParseConfig(desk.ConfDir.GetPath("/system.json"))

	if err != nil {
		Logger.Printf("System File Not Found: %v", err)
		return
	}

	desk.SystemConfig = &readConfig

	c.Init()

	if c.Error != nil {
		Logger.Fatalf("Failed to initialize Core: %v", c.Error)
		return
	}

	if desk.Core == nil {
		Logger.Fatalf("desk.Core is nil")
	}

	if desk.Core.Connection == nil {
		Logger.Fatalf("desk.Core.Connection is nil")
	}

	desk.Setup = xproto.Setup(desk.Core.Connection.Conn())
	desk.Screen = desk.Setup.DefaultScreen(desk.Core.Connection.Conn())
	desk.Root = desk.Screen.Root

	Logger.Printf("Theme: %s", desk.GetThemePath())

	// Autostart
	desk.AutoStart()
	desk.CreateBar()

	// Set up keybindings
	desk.SetupKeybindings()
}

// SetupKeybindings configures keybindings, such as launching a terminal
func (desk *Desktop) SetupKeybindings() {
	
	keybind.Initialize(desk.Core.Connection)
	mousebind.Initialize(desk.Core.Connection)

	for _, shortcut := range desk.SystemConfig.Shortcuts {
		desk.SetupKeybinding(shortcut.Key, shortcut.Command)
	}

	// Event loop to handle X11 events
	for {
		ev, err := desk.Core.Connection.Conn().WaitForEvent()
		if err != nil {
			Logger.Printf("Event error: %v", err)
			break
		}

		// Event işleyici
		switch ev := ev.(type) {
		case xproto.CreateNotifyEvent:
			Logger.Printf("New window created: %d", ev.Window)
			desk.SetWindowBorder(ev.Window, 4, 0x00FF00) // 4 piksel yeşil kenarlık
		case xproto.KeyPressEvent:
			Logger.Printf("KeyPressEvent: %v", ev)
			desk.HandleKeyPressEvent(ev)
		}
	}
}

func (desk *Desktop) AutoStart() {
	for _, startup := range desk.SystemConfig.AutoStart {
		Logger.Printf("Executing: %v", startup.Command)
		switch startup.Type{
			case "command":
				desk.LaunchApp(startup.Command)
			case "system":
				desk.ConfDir.Exec(startup.Command)
			default:
				desk.LaunchApp(startup.Command)
		}
	}
}

func (desk *Desktop) HandleKeyPressEvent(ev xproto.KeyPressEvent) {
	Logger.Printf("KeyPressEvent: KeyCode: %d, Detail: %d", ev.Detail, ev.Time)

	// KeySym'e dönüştür
	keysym := desk.GetKeysymFromEvent(ev)
	Logger.Printf("Keysym: %v", keysym)

	// Kısayolu kontrol et ve uygula
	for _, shortcut := range desk.SystemConfig.Shortcuts {
		if desk.IsMatchingShortcut(shortcut, ev, keysym) {
			Logger.Printf("Type: %v", shortcut.Type)
			switch shortcut.Type{
				case "command":
					desk.LaunchApp(shortcut.Command)
				case "system":
					desk.ConfDir.Exec(shortcut.Command)
				default:
					desk.LaunchApp(shortcut.Command)
			}
		}
	}
}

func (desk *Desktop) GetKeysymFromEvent(ev xproto.KeyPressEvent) xproto.Keysym {
	// Keycode'u Keysym'e dönüştür
	keycode := ev.Detail
	keysym, err := desk.GetKeysymForKeycode(keycode)
	if err != nil {
		Logger.Printf("Error getting keysym: %v", err)
		return 0
	}
	return keysym
}


func (desk *Desktop) GetThemePath() string {
	if desk.SystemConfig != nil {
		return desk.ConfDir.GetPath("/themes/"+desk.SystemConfig.Theme)
	}
	return desk.ConfDir.GetPath("/themes/default")
}

func (desk *Desktop) CreateBar() {
	// Yeni pencere id'sini oluştur
	win, _ := xproto.NewWindowId(desk.Core.Connection.Conn())

	// Ekran boyutlarını al
	screen := desk.Screen
	width := screen.WidthInPixels
	height := screen.HeightInPixels

	// Barın yüksekliği
	barHeight := uint16(50)

	// Pencerenin boyutlarını ve konumunu ayarla
	x := int16(0) // X konumu, sol kenarda (int16 olarak ayarladık)
	y := int16(height - barHeight) // Y konumu, ekranın alt kısmı

	// Pencereyi oluştur
	xproto.CreateWindow(desk.Core.Connection.Conn(), desk.Screen.RootDepth, win, desk.Root, x, y, uint16(width), barHeight, 0,
		xproto.WindowClassInputOutput, desk.Screen.RootVisual,
		xproto.CwBackPixel|xproto.CwEventMask,
		[]uint32{desk.Screen.WhitePixel, xproto.EventMaskExposure})

	// Pencereyi göster
	xproto.MapWindow(desk.Core.Connection.Conn(), win)

	// Bar öğelerini eklemek için loop
	for _, barItem := range desk.SystemConfig.Bar {
		// İkonu yükle
		iconPath := barItem.Icon
		icon := desk.LoadIcon(desk.GetThemePath()+"/icons/"+iconPath)

		// Bar öğesini pencereye ekleyin
		desk.AddBarItem(win, barItem.Name, icon.Path)
	}
}

func (desk *Desktop) SetWindowBorder(win xproto.Window, borderWidth uint32, borderColor uint32) {
    xproto.ConfigureWindow(desk.Core.Connection.Conn(), win, xproto.ConfigWindowBorderWidth, []uint32{borderWidth})
    xproto.ChangeWindowAttributes(desk.Core.Connection.Conn(), win, xproto.CwBorderPixel, []uint32{borderColor})
}

func (desk *Desktop) DrawIconOnWindow(win xproto.Window, iconPath string, x, y, width, height uint16) {
	icon := desk.LoadIcon(iconPath)
	if icon == nil {
		Logger.Printf("Icon not found: %s", iconPath)
		return
	}
	// İkon çizme işlemi yapılacak (Bu basit örnek, X11'e özel çizim işlemlerini içerebilir)
	Logger.Printf("Icon %s drawn at %d, %d", iconPath, x, y)
	// XRender veya XImage kullanarak ikonu gerçekten çizebilirsiniz.
}

// Bar öğesini pencereye ekleyin
func (desk *Desktop) AddBarItem(win xproto.Window, name string, iconPath string) {
	Logger.Printf("Adding Bar item: %v with Icon %v", name, iconPath)

	xPos, yPos := 10, 10 // Başlangıç pozisyonları
	if iconPath != "" {
		// PNG'yi yükleyip pencereye çiz
		err := desk.DrawPNG(win, iconPath, xPos, yPos)
		if err != nil {
			Logger.Printf("Failed to draw PNG icon: %v", err)
		} else {
			// PNG'nin genişliği kadar boşluk bırak
			img, _ := loadImage(iconPath)
			if img != nil {
				xPos += img.Bounds().Dx() + 5
			}
		}
	}

	// Metin (name) için bir font yükleyin
	font := desk.LoadFont("fixed") // X11'in 'fixed' fontu örnek olarak kullanılabilir
	if reflect.TypeOf(font) != nil {
		defer desk.CloseFont(font)
		textY := yPos + 15
		desk.DrawText(win, font, xPos, textY, name)
	} else {
		Logger.Printf("Failed to load font for text rendering.")
	}

	
	Logger.Printf("Bar Item Added: %v, %s, %s", win, name, iconPath)

	// Pencereyi güncelle
	desk.RefreshWindow(win)
}

// DrawPNG, bir PNG dosyasını pencereye çizer
func (desk *Desktop) DrawPNG(win xproto.Window, filePath string, x, y int) error {
	img, err := loadImage(filePath)
	if err != nil {
		return err
	}

	// Görüntüyü X11 pixmap'e dönüştür
	pixmap, err := desk.CreatePixmapFromImage(win, img)
	if err != nil {
		return err
	}

	// Pixmap'i pencereye çiz
	desk.DrawPixmap(win, pixmap, x, y, img.Bounds().Dx(), img.Bounds().Dy())
	return nil
}

// CreatePixmapFromImage, bir PNG'yi Pixmap'e dönüştürür
func (desk *Desktop) CreatePixmapFromImage(win xproto.Window, img image.Image) (xproto.Pixmap, error) {
	width := img.Bounds().Dx()
	height := img.Bounds().Dy()

	// Pixmap oluştur
	pixmap, _ := xproto.NewPixmapId(desk.Core.Connection.Conn())
	err := xproto.CreatePixmapChecked(desk.Core.Connection.Conn(), 24, pixmap, xproto.Drawable(win), uint16(width), uint16(height)).Check()
	if err != nil {
		return 0, err
	}

	// Görüntüyü Pixmap'e kopyala
	gc, _ := xproto.NewGcontextId(desk.Core.Connection.Conn())
	xproto.CreateGC(desk.Core.Connection.Conn(), gc, xproto.Drawable(pixmap), xproto.GcGraphicsExposures, []uint32{0})

	// Görüntüyü Pixmap'e çizin
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			r, g, b, _ := img.At(x, y).RGBA()
			// RGB değerlerini 8 bit olarak hesaplayın
			red := uint8(r >> 8)
			green := uint8(g >> 8)
			blue := uint8(b >> 8)

			// X11 formatına uygun renk oluşturun
			color := uint32(red)<<16 | uint32(green)<<8 | uint32(blue)

			// `color` değerini gc (Graphics Context) ile ilişkilendiriyoruz
			xproto.ChangeGC(desk.Core.Connection.Conn(), gc, xproto.GcForeground, []uint32{color})

			// Pikselleri Pixmap'e doldurun
			xproto.PolyFillRectangle(desk.Core.Connection.Conn(), xproto.Drawable(pixmap), gc, []xproto.Rectangle{
				{
					X:      int16(x),
					Y:      int16(y),
					Width:  1,
					Height: 1,
				},
			})
		}
	}

	// Kaynakları temizle
	xproto.FreeGC(desk.Core.Connection.Conn(), gc)

	return pixmap, nil
}

// DrawPixmap, Pixmap'i pencereye çizer
func (desk *Desktop) DrawPixmap(win xproto.Window, pixmap xproto.Pixmap, x, y, width, height int) {
	gc,_ := xproto.NewGcontextId(desk.Core.Connection.Conn())
	xproto.CreateGC(desk.Core.Connection.Conn(), gc, xproto.Drawable(win), xproto.GcGraphicsExposures, []uint32{0})
	xproto.CopyArea(desk.Core.Connection.Conn(), xproto.Drawable(pixmap), xproto.Drawable(win), gc, 0, 0, int16(x), int16(y), uint16(width), uint16(height))
	xproto.FreeGC(desk.Core.Connection.Conn(), gc)
}

// DrawText, verilen fontla bir metni pencereye çizer
func (desk *Desktop) DrawText(win xproto.Window, font xproto.Font, x, y int, text string) {
	// Graphics Context (gc) oluşturun
	gc, _ := xproto.NewGcontextId(desk.Core.Connection.Conn())

	// GC'yi oluşturun ve fontu atayın
	// Burada font, uint32 türünde olmalı
	xproto.CreateGC(desk.Core.Connection.Conn(), gc, xproto.Drawable(win), xproto.GcFont, []uint32{uint32(font)})

	// Metni çizmek için ImageText8 fonksiyonunu çağırın
	// gc'yi ve fontu doğru parametrelerle geçirin
	xproto.ImageText8(desk.Core.Connection.Conn(), byte(len(text)), xproto.Drawable(win), gc, int16(x), int16(y), text)

	// GC'yi serbest bırakın
	xproto.FreeGC(desk.Core.Connection.Conn(), gc)
}

// LoadFont, verilen bir fontu yükler
func (desk *Desktop) LoadFont(fontName string) xproto.Font {
	font,_ := xproto.NewFontId(desk.Core.Connection.Conn())
	err := xproto.OpenFontChecked(desk.Core.Connection.Conn(), font, uint16(len(fontName)), fontName).Check()
	if err != nil {
		Logger.Printf("Failed to load font: %v", err)
		return 0
	}
	return font
}

// CloseFont, bir fontu kapatır
func (desk *Desktop) CloseFont(font xproto.Font) {
	xproto.CloseFont(desk.Core.Connection.Conn(), font)
}

// RefreshWindow, pencereyi yeniden çizer
func (desk *Desktop) RefreshWindow(win xproto.Window) {
	xproto.ClearArea(desk.Core.Connection.Conn(), true, win, 0, 0, 0, 0)
}

// loadImage, bir PNG dosyasını yükler ve `image.Image` olarak döner
func loadImage(filePath string) (image.Image, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	img, err := png.Decode(file)
	if err != nil {
		return nil, err
	}
	return img, nil
}

// İkonu yüklemek için bir örnek fonksiyon
func (desk *Desktop) LoadIcon(iconPath string) *types.Image {
	// İkonu yükleme ve dönüştürme işlemini burada gerçekleştirebilirsiniz.
	// Bu örnekte, ikonun yolunu alıyoruz ve bir Image türünde döndürüyoruz.
	Logger.Printf("Loading icon from: %s", iconPath)
	return &types.Image{Path: iconPath} // Burada Image türü örnektir.
}


func (desk *Desktop) GetKeysymForKeycode(keycode xproto.Keycode) (xproto.Keysym, error) {
	setup := xproto.Setup(desk.Core.Connection.Conn())
	minKeycode := setup.MinKeycode
	maxKeycode := setup.MaxKeycode

	reply, err := xproto.GetKeyboardMapping(desk.Core.Connection.Conn(), minKeycode, uint8(maxKeycode-minKeycode+1)).Reply()
	if err != nil {
		return 0, fmt.Errorf("failed to get keyboard mapping: %v", err)
	}

	// Debug: Print keycodes and keysyms in the keyboard mapping
	for i := 0; i < len(reply.Keysyms); i++ {
		expectedKeycode := xproto.Keycode(uint8(int(minKeycode) + i))
		Logger.Printf("Checking Keycode: %d, Expected KeySym: %v, Got KeySym: %v", expectedKeycode, expectedKeycode, reply.Keysyms[i])

		if expectedKeycode == keycode {
			if reply.Keysyms[i] == 0 {
				return 0, fmt.Errorf("keycode %d has no associated KeySym", keycode)
			}
			return reply.Keysyms[i], nil
		}
	}

	return 0, fmt.Errorf("keycode %d not found in keyboard mapping", keycode)
}

func (desk *Desktop) StringToKeycode(key string) (xproto.Keycode, error) {
	// "Control-Mod1-r" gibi bir tuş kombinasyonu için parçalama
	parts := strings.Split(key, "-")
	var keySyms []xproto.Keysym

	// Her parçayı kontrol et
	for _, part := range parts {
		keysym := StringToKeysym(part)
		if keysym == 0 {
			return 0, fmt.Errorf("unknown key: %s", part)
		}
		keySyms = append(keySyms, keysym)
	}

	// İlk KeySym'i al ve onu Keycode'a çevir
	// Örneğin, "Control" + "Mod1" + "r" tuşları birleşiminde ilk olarak 'r' tuşu kontrol edilebilir
	return desk.KeysymToKeycode(keySyms[len(keySyms)-1]) // Sonuncu tuşu kontrol ediyoruz
}

func (desk *Desktop) IsMatchingShortcut(shortcut Shortcut, ev xproto.KeyPressEvent, keysym xproto.Keysym) bool {
	// Kısayol eşleşmesini kontrol et
	// Modifiers kontrolü de yapılabilir (örneğin Control, Mod1 vs.)
	expectedKeysym := desk.GetKeysymFromName(shortcut.Key)
	return keysym == expectedKeysym
}

func (desk *Desktop) SetupKeybinding(key string, command string) {
	// Keybinding kurulumunu yap
	keybind.KeyPressFun(func(X *xgbutil.XUtil, e xevent.KeyPressEvent) {
		desk.LaunchApp(command)
	}).Connect(desk.Core.Connection, desk.Core.Connection.RootWin(), key, true)

	Logger.Printf("Shortcut: %s -> %s\n", key, command)
}


func (desk *Desktop) LaunchApp(command string) {
    Logger.Printf("Launching application: %v", command)
    
    // Create a new window
    win, err := xwindow.Create(desk.Core.Connection, desk.Core.Connection.RootWin())
    if err != nil {
        Logger.Printf("Window creation failed: %v", err)
        return
    }

    // Set window properties
    win.Change(xproto.CwBackPixel, 0x000000) // Black background
    borderWidth := uint32(4)                 // Border width
    win.MoveResize(100, 100, int(600-borderWidth*2), int(400-borderWidth*2))
    desk.SetWindowBorder(win.Id, borderWidth, 0x00FF00)
    win.Map() // Map the window to make it visible

	// Local onDragBegin function
    onDragBegin := func(xu *xgbutil.XUtil, rootX, rootY, eventX, eventY int) (bool, xproto.Cursor) {
        Logger.Printf("Drag started at: (%d, %d)", eventX, eventY)

        // Here you can set a cursor for the drag (e.g., a cross cursor)
        cursor := xproto.Cursor(0) // Change this to an actual cursor ID as needed
        return false, cursor // Returning false means the drag is not cancelled
    }

    // Local onDragging function
    onDragging := func(xu *xgbutil.XUtil, rootX, rootY, eventX, eventY int) {
        Logger.Printf("Dragging to: (%d, %d)", eventX, eventY)

        // Move the window according to the drag position (eventX and eventY are root coordinates)
        xproto.ConfigureWindow(xu.Conn(), win.Id, xproto.ConfigWindowX|xproto.ConfigWindowY, []uint32{uint32(eventX), uint32(eventY)})
    }

    // Local onDragEnd function
    onDragEnd := func(xu *xgbutil.XUtil, rootX, rootY, eventX, eventY int) {
        Logger.Printf("Drag ended at: (%d, %d)", eventX, eventY)
    }

    // Pass them to the Drag function:
    mousebind.Drag(
        desk.Core.Connection,             // XUtil instance
        desk.Core.Connection.RootWin(),   // Window to grab
        win.Id,                           // Window to drag
        "1",                              // Mouse button (left-click)
        true,                             // Grab pointer
        onDragBegin,                      // Drag start handler
        onDragging,                       // Drag move handler
        onDragEnd,                        // Drag end handler
    )

    desk.Run(command)
}



func (desk *Desktop) Run(command string) {
	go func() {
		cmd := exec.Command(command)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err := cmd.Start()
		if err != nil {
			Logger.Printf("Failed to launch application %v: %v", command, err)
		}
	}()
}

func (desk *Desktop) GetKeyCodeForKeySym(keySym xproto.Keysym) (xproto.Keycode, error) {
	// Minimum ve maksimum keycode aralığını al
	setup := xproto.Setup(desk.Core.Connection.Conn())
	minKeycode := setup.MinKeycode
	maxKeycode := setup.MaxKeycode

	// Klavye haritasını al
	reply, err := xproto.GetKeyboardMapping(desk.Core.Connection.Conn(), minKeycode, uint8(maxKeycode-minKeycode+1)).Reply()

	if err != nil {
		return 0, err
	}

	// Keymap'e bakarak keysym eşleşmesini bul
	for i := 0; i < len(reply.Keysyms); i++ {
		ks := reply.Keysyms[i]
		if ks == keySym {
			return xproto.Keycode(uint8(int(minKeycode) + i)), nil
		}
	}
	return 0, fmt.Errorf("keycode not found: %d", keySym)
}

func (desk *Desktop) GetKeysymFromName(name string) xproto.Keysym {
	switch name {
	case "t":
		return xproto.Keysym(0x0074) // 't' tuşunun KeySym değeri
	case "r":
		return xproto.Keysym(0x0072) // 'r' tuşunun KeySym değeri
	case "x":
		return xproto.Keysym(0x0078) // 'x' tuşunun KeySym değeri
	default:
		return 0
	}
}

func (desk *Desktop) ParseConfig(filePath string) (Config, error) {
	data, err := os.ReadFile(filePath)
	Logger.Printf("ConfigFile: %v\n", filePath)
	emptyConf := Config{}
	if err != nil {
		return emptyConf, err
	}

	var config Config
	if err := json.Unmarshal(data, &config); err != nil {
		return emptyConf, err
	}

	Logger.Printf("ConfigFile: %v\n", config)

	// Geri döndürülecek kısayolları hazırla
	return config, nil
}

// Keysym'i Keycode'a çeviren bir fonksiyon
func (desk *Desktop) KeysymToKeycode(keysym xproto.Keysym) (xproto.Keycode, error) {
	// X sunucusunun klavye ayarlarını alıyoruz
	setup := xproto.Setup(desk.Core.Connection.Conn())
	minKeycode := setup.MinKeycode
	maxKeycode := setup.MaxKeycode

	// Klavye haritasını getir
	reply, err := xproto.GetKeyboardMapping(desk.Core.Connection.Conn(), minKeycode, uint8(maxKeycode-minKeycode+1)).Reply()
	if err != nil {
		return 0, fmt.Errorf("failed to get keyboard mapping: %v", err)
	}

	// Keysym değerini Keycode ile eşleştir
	for i, syms := range reply.Keysyms {
		if syms == keysym {
			return xproto.Keycode(uint8(int(minKeycode) + i)), nil
		}
	}

	return 0, fmt.Errorf("keycode not found for keysym: %d", keysym)
}

func StringToKeysym(key string) xproto.Keysym {
	keysymMap := map[string]xproto.Keysym{
		"a":       0x0061, // 'a'
		"b":       0x0062, // 'b'
		"Control": 0xffe3, // Control tuşu
		"Alt":     0xffe9, // Alt tuşu (Mod1)
		"Shift":   0xffe1, // Shift tuşu
		"t":       0x0074, // 't'
		"x":       0x0078, // 'x'
		"r":       0x0072, // 'r'
		"Mod1":    0xffe9, // Alt tuşu, Mod1
		"Mod4":    0xffe7, // Super tuşu, Mod4
	}

	if keysym, found := keysymMap[key]; found {
		return keysym
	}

	// Hata durumunda debug mesajı ekleyin
	Logger.Printf("StringToKeysym: Unknown key %s", key)
	return 0
}
