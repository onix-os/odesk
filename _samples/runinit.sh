#!/bin/env bash

export ODESK=$HOME/.config/onixos/odesk
export OPATH=/usr/bin

if [ ! -d $ODESK ]
then
    mkdir -p $ODESK
    if [ ! -f $ODESK/startup.log ]
    then
        touch $ODESK/startup.log
    fi
fi

exec $OPATH/odesk > $ODESK/startup.log 2>&1